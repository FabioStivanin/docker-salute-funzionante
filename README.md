# Prima bozza DOCKER catalogSalute


### Follow these steps to run the Docker image
1. git clone <git_url>
2. cd <folder>
3. ./build_local.sh # it will build the images needed by docker-compoose
4. docker-compose up -d # it will run all the needed containers
5. ./init.sh # it will configure the plugins and it will create all the vocabolaries

Then you can open the ckan home [http://localhost:8080](http://localhost:8080).
The init.sh script creates an admin user with the following credentials: sanita/sanita


### Comandi utili

> docker inspect -f "{{json .Mounts}}" ckan-apache | jq '.'
 (va installato prima jq che formatta il json) 

> docker exec -i -t ckan-apache /bin/bash
(entra nel docker e lancia il comando passato alla fine nella fattispecie qui apro shell dentro docker)

> docker build <variabili proxy> <nome immagine> <directotry root del contesto>
in  build-local.sh
(fa la build delle immagini che servono: ckan - solr)

> docker-compose up -d
(esegue il file docker-compose.yml )
	vim docker-compose.yml
	( viene eseguito al contrario!!!!)
	specifico qui 
	- il servizio
	- il nome
	- link ad altri 3 container (db, solr, redis) che specifico sotto ( in image si collega al repository dei docker online e prova scaricarla) 
	- le porte
	- variabili d'ambiente ( riesco a aspecificare direttamente qui dove sta solr redis e il db di ckan!!!!)

> docker ps -a
(mostra tutti i container e loro stato)
 es
- ckan-apache
- solr

> docker stop $(docker ps -q)
stoppa tutti quelli attivi

> docker rm $(docker ps -a -q)
cancella tutti container fermi

> docker rmi $(docker images -a)
rimuove immagini

> docker logs ckan-apache
per vedere i log di docker


### Log errore CKAN

docker exec -ti ckan-apache bash -c "vim var/log/apache2/ckan_default.error.log"




