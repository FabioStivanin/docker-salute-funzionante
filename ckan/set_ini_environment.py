
import os
import ConfigParser

# Script per settare le variabili d'ambiente riguardanti SOLR e il DB POSTGRESQL
# Queste vengono dichiarate nel docker-compose

# Utilizza il modulo ConfigParser per scrivere su ckan.ini
# DOCS https://docs.python.org/3/library/configparser.html


# leggo il file ckan.ini ed aggiungo in coda nella sezione [app:main] le variabili seguenti:
config = ConfigParser.RawConfigParser()
config.read('/etc/ckan/default/ckan.ini')

config.set('app:main', 'solr_url', os.environ["SOLR_URL"])
config.set('app:main', 'solr_user', os.environ["SOLR_USER"])
config.set('app:main', 'solr_password', os.environ["SOLR_PASSWORD"])
config.set('app:main', 'sqlalchemy.url', os.environ["SQLALCHEMY_URL"])
config.set('app:main', 'ckan.datastore.write_url', os.environ["DATASTORE_WRITE_URL"])
config.set('app:main', 'ckan.datastore.read_url', os.environ["DATASTORE_READ_URL"])
config.set('app:main', 'ckan.root_path', os.environ["CKAN_ROOT_PATH"])

with open('/etc/ckan/default/ckan.ini', 'w') as configfile:
	config.write(configfile)

