import logging
import uuid
import requests

from pylons import session

import ckan.plugins as p
import ckan.lib.helpers as h
import ckan.model as model
import ckan.logic.schema as schema

t = p.toolkit
log = logging.getLogger(__name__)


def _no_permissions(context, msg):
	user = context['user']
	return {'success': False, 'msg': msg.format(user=user)}


@t.auth_sysadmins_check
def user_create(context, data_dict):
	msg = p.toolkit._('Users cannot be created.')
	return _no_permissions(context, msg)


@t.auth_sysadmins_check
def user_update(context, data_dict):
	msg = p.toolkit._('Users cannot be edited.')
	return _no_permissions(context, msg)


@t.auth_sysadmins_check
def user_reset(context, data_dict):
	msg = p.toolkit._('Users cannot reset passwords.')
	return _no_permissions(context, msg)


@t.auth_sysadmins_check
def request_reset(context, data_dict):
	msg = p.toolkit._('Users cannot reset passwords.')
	return _no_permissions(context, msg)

"""
ESTENSIONE PER AUTENTIZAZIONE TRAMITE IAM
sulla falsa riga dell'estensione ckanext-ldap (https://github.com/whythawk/ckanext-ldap)

NB: per abilitare aggiungere iam ai plugins nel file di config in etc/ckan/development.ini TODO

"""
class IamPlugin(p.SingletonPlugin):
	p.implements(p.IAuthenticator, inherit=True)
	p.implements(p.IAuthFunctions, inherit=True)
	p.implements(p.IConfigurer)
	#p.implements(p.IActions)
	
	# pagina di login diversa in iam/templates/user
	def update_config(self, config):
		t.add_template_directory(config, 'templates')

	# main login method
	def login(self):

		#log.info('\nLOGIN\n')
		username = t.request.headers.get('username')
		#log.info(username)
		key = t.request.headers.get('key')
		#log.info(key)
		IAM_KEY = 'CVFjp4HLjkCZhgY7Cnn4vEKcN9nhPB7T'
		#username = t.request.POST.get('login') # form
		#password = t.request.POST.get('password') #form
		
		if (username and key == IAM_KEY):
			#log.info('username and key == IAM_KEY  is TRUE')
			userobj = model.User.get(username)
			if userobj:
				#############################
				#log.info(userobj)
				if userobj.sysadmin:
					#log.info ('\n\nis sysadmin')
					model.Session.add(userobj)
					model.Session.commit()
				##############################

				session['iam_user'] = username
				session.save()
				
				#log.info(h.redirect_to(controller='user', action='dashboard'))
				h.redirect_to(controller='user', action='dashboard')
				#h.redirect_to('//dashboard')
			else:
				h.flash_error('Inserisci un username valido in CKAN.')
		else:
			log.info('username and key == IAM_KEY  is FALSE')


	def logout(self):
		session['iam_user'] = None
		session.delete()

	def identify(self):

		#log.info('\nIDENTIFY\n')
		username = t.request.headers.get('username') ###
		#log.info('username: '+str(username))
		iam_user = session.get('iam_user')
		#log.info('iam_user: '+str(iam_user))
		c = t.c
		if iam_user:
			"""
			if iam_user != username:
				log.info('redirect to login method')
				h.redirect_to(controller='user', action='login')
			"""
			c.userobj = model.User.get(iam_user)
			c.user = iam_user
		#log.info('\nend identify\n')

	def get_auth_functions(self):
		# we need to prevent some actions being authorized.
		return {
			'user_create': user_create,
			#'user_update': user_update,
			'user_reset': user_reset,
			'request_reset': request_reset
		}
