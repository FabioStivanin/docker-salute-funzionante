# encoding: utf-8
import re
import os
import ckan.plugins as p
import ckan.plugins.toolkit as tk
import ckan.model as model
import ckan.logic.validators as validators
from ckan.common import _
import ckan.lib.navl.dictization_functions as df
missing = df.missing

############################### validatori vari
def set_value(default_value):
    def callable(key, data, errors, context):

        data[key] = default_value

    return callable

def default_public(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = 'public'

def default_si(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = 'SI'

def default_NA(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = 'N/A'

def default_identifier(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = str(data[('holder_identifier',)]) + ":" + str(data[('name',)])

def default_theme(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = 'ENVI'

def default_frequency(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = 'IRREG'

def default_language(key, data, errors, context):
    value = data.get(key, None)
    if not value or value is missing:
        data[key] = 'ITA'



###############################
# MAP TO DCATAPIT METADATA
###############################
def converter_to_dcatapit(key, data, errors, context):

    tmp = data[key]
    
    if key[-1]== 'ico_categoria_eurovoc': 
        key = ('sub_theme',) #map to sub_theme
    
    elif key[-1]== 'ico_area_geografica':
        key = ('geographical_name',) # map to geographical_name

    elif key[-1]== 'ico_url_area_geografica':
        key = ('geographical_geonames_url',) # map to geographical_geonames_url

    elif key[-1]== 'ico_rightsHolder':
        key = ('holder_name',) # map to holder_name

    elif key[-1]== 'ico_info_aggiuntive':
        key = ('url',) # map to url
    
    elif key[-1]== 'data_di_pubblicazione_dato':
        key = ('issued',) # map to issued
    
    elif key[-1]== 'data_di_aggiornamento_dato':
        key = ('modified',) # map to modified


    current_indexes = [k[1] for k in data.keys() if len(k) > 1 and k[0] == 'extras']
    
    new_index = max(current_indexes) + 1 if current_indexes else 0

    data[('extras', new_index, 'key')] = key[-1]
    data[('extras', new_index, 'value')] = tmp 



###############################
# MAP ICO_XXX TO DCAT METADATA
###############################
def converter_from_ico_to_dcat (key, data, errors, context):

    #print "\nCONVERTER FROM ICO TO DCAT\n"
    tmp = data[key]
    

    if key[-1]== 'ico_lingua': 
        key = ('language',) #MAP TO LANGUAGE
    
    elif key[-1]== 'ico_temporal_start':
        key = ('temporal_start',) #MAP TO TEMPORAL_START
    
    elif key[-1]== 'ico_temporal_end':
        key = ('temporal_end',) #MAP TO TEMPORAL_END
    
    elif key[-1]== 'ico_frequenza_aggiornamento':
        key = ('frequency',) #MAP TO FREQUENCY (nuovo nome metadato)
        
        new_value = "UNKNOWN"
        frequencies_mapping = {
                "annually": "ANNUAL",
                "biannually": "BIENNIAL",
                "fortnightly": "BIWEEKLY",
                "continual": "CONT",
                "daily": "DAILY",
                "irregular": "IRREG",
                "monthly": "MONTHLY",
                "asNeeded" : "OTHER",
                "notPlanned": "OTHER",
                "quarterly": "QUARTERLY",
                "unknown": "UNKNOWN",
                "weekly": "WEEKLY"
            }
        if tmp in frequencies_mapping:
            new_value = frequencies_mapping[tmp]

        tmp = new_value
    
    elif key[-1]== 'ico_conformsTo':
        key = ('conforms_to',) #MAP TO CONFORM
    
    elif key[-1]== 'ico_nome_ente_responsabile':
        key = ('holder_name',) #MAP TO RIGHTS HOLDER

    elif key[-1]== 'ico_nome_struttura_riferimento':
        key = ('publisher_name',) #MAP TO PUBLISHER (EDITOR)
    
    elif key[-1]== 'ico_isVersionOf':
        key = ('is_version_of',) #MAP TO VERSION


    current_indexes = [k[1] for k in data.keys() if len(k) > 1 and k[0] == 'extras']
    
    new_index = max(current_indexes) + 1 if current_indexes else 0    

    data[('extras', new_index, 'key')] = key[-1]
    data[('extras', new_index, 'value')] = tmp 



